import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import itertools

from sklearn.metrics import f1_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from imblearn.pipeline import Pipeline as ImbPipeline
from sklearn.decomposition import TruncatedSVD
from xgboost import XGBClassifier


def import_data(path, dict_infos_columns):
    """Import data as a dataframe"""
    data = pd.read_csv(
        path,
        header=None,
        names=list(dict_infos_columns.keys()),
        dtype=dict_infos_columns,
    )
    data.columns = [c.strip().replace(" ", "_") for c in data.columns]
    return data


def data_preparation(data):
    """Operations of data preparation for income range classification"""
    ## missing values
    missing_values_mapping = ["Not identifiable", "NA", "Do not know", " ?", "?"]
    # replace missing values by np.nan
    data = data.replace(missing_values_mapping, np.nan)

    ## drop the migration column with many np.nan
    migration_col = [col for col in data.columns if "migration" in col]
    data = data.drop(migration_col, axis=1)

    ## drop features
    # Drop the year column (94, 95)
    data = data.drop(["year"], axis=1)
    # drop the wage_per_hour column that appear inconsistent
    data = data.drop("wage_per_hour", axis=1)

    # adult filter : keep only > -16 years old values
    adult_mask = lambda x: x["age"] >= 16
    data = data[adult_mask(data)]

    return data


def get_estimator(estimator, parameters):
    """Returns a sklearn estimator object to be fitted"""
    random_state = 42
    if estimator == "SVC":
        if parameters is not None:
            return SVC(random_state=random_state, **parameters)
        else:
            return SVC(random_state=random_state)
    elif estimator == "LR":
        if parameters is not None:
            return LogisticRegression(random_state=random_state, **parameters)
        else:
            return LogisticRegression(random_state=random_state)
    elif estimator == "RFC":
        if parameters is not None:
            return RandomForestClassifier(random_state=random_state, **parameters)
        else:
            return RandomForestClassifier(random_state=random_state)
    elif estimator == "XGB":
        if parameters is not None:
            return XGBClassifier(random_state=random_state, **parameters)
        else:
            return XGBClassifier(random_state=random_state)
    else:
        print("unknown sklearn estimator")


def get_pipeline(preprocessor, estimator, estimator_parameters):
    """Returns an imbalanced-learn pipeline with the estimator and its parameters"""
    return ImbPipeline(
        [
            ("feature_eng", preprocessor),
            (
                "classifier",
                get_estimator(estimator, estimator_parameters),
            ),
        ]
    )


def get_pipeline_svd(preprocessor, estimator, estimator_parameters):
    """Returns an imbalanced-learn pipeline with the estimator and its parameters"""
    return ImbPipeline(
        [
            ("feature_eng", preprocessor),
            ("SVD", TruncatedSVD(n_components=30)),
            (
                "classifier",
                get_estimator(estimator, estimator_parameters),
            ),
        ]
    )


def manual_weighted_f1_score_gridsearch(
    preprocessor,
    estimator,
    tmp_model_param,
    combinations_parameters,
    X_train,
    y_train,
    X_test,
    y_test,
    train_sample_weights,
    test_sample_weights,
    svd_step=False,
):
    """Manual Grid Search to take into account the sample weights in the scoring & training with f1 scoring"""
    scores_model = {}
    for combination in list(itertools.product(*list(combinations_parameters.values()))):
        tmp_model_param.update(dict(zip(combinations_parameters.keys(), combination)))
        print("Hyperparameters: ", tmp_model_param)
        if svd_step:
            tmp_imb_model = get_pipeline_svd(
                preprocessor=preprocessor,
                estimator=estimator,
                estimator_parameters=tmp_model_param,
            )
        else:
            # define pipeline
            tmp_imb_model = get_pipeline(
                preprocessor=preprocessor,
                estimator=estimator,
                estimator_parameters=tmp_model_param,
            )
        tmp_imb_model.fit(
            X_train, y_train, classifier__sample_weight=train_sample_weights
        )
        y_predict = tmp_imb_model.predict(X_test)
        scores_model[combination] = np.round(
            f1_score(y_test, y_predict, sample_weight=test_sample_weights), 3
        )
    return scores_model
