import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from statsmodels.stats.weightstats import DescrStatsW
from sklearn.metrics import confusion_matrix

# Descritive analysis
def get_quantile_val(data, column):
    """Compute  0.25 0.5 0.75 quantiles values of a variable"""
    _wdf = DescrStatsW(data[column], weights=data["instance_weight"], ddof=1)
    _quantile_values = _wdf.quantile([0.25, 0.5, 0.75])
    return _quantile_values


def plot_weigthed_histogram(data, column, bins=15, figsize=(8, 6)):
    """Plot repartition histogram of a quantitative variable"""
    _quantile_values = get_quantile_val(data, column)
    fig = plt.figure(figsize=figsize)
    plt.hist(data[column], bins=bins, weights=data["instance_weight"].values)
    plt.xlabel(column)
    plt.ylabel("Occurences")
    plt.axvline(
        _quantile_values[0.5],
        color="k",
        linestyle="dashed",
        linewidth=1,
        label=f"Median: {_quantile_values[0.5]}",
    )
    plt.axvline(
        _quantile_values[0.25],
        color="darkgrey",
        linestyle="dashed",
        linewidth=1,
        label=f"Quantile 0.25: {_quantile_values[0.25]}",
    )
    plt.axvline(
        _quantile_values[0.75],
        color="darkgrey",
        linestyle="dashed",
        linewidth=1,
        label=f"Quantile 0.75: {_quantile_values[0.75]}",
    )
    plt.legend()
    plt.title(f"Histogram of repartition of the {column} values in the training set")
    return fig


def barplot_share_per_modality(data, column, figsize=(6, 4)):
    """Barplot of percentage share per modality of a variable"""
    fig = plt.figure()
    (
        100
        * data.groupby(column)["instance_weight"].sum()
        / data["instance_weight"].sum()
    ).plot(
        kind="barh",
        title=f"Percentage share per modality of the {column} variable",
        figsize=figsize,
    )
    return fig


def under_fifty_prop_per_value(data, column, income_range="under"):
    """Return a pd.serie with part of persons with under 50k of income per value of a column"""
    return data.groupby(["income_range", column])["instance_weight"].sum().unstack(0)[
        income_range
    ] / ((data.groupby(column)["instance_weight"].sum()).transpose())


def repartition_under_over_per_category(data, column):
    """Returns a pd.df with over/under 50k percentage repartition per category value of the arg column"""
    return (
        data.groupby([column, "income_range"])["instance_weight"]
        .sum()
        .unstack([column])
        .apply(lambda x: x * 100 / x.sum(), axis=1)
    )


# modeling
def plot_confusion_matrix(y_test, y_pred, test_sample_weight, title):
    """Plot confusion matrix"""
    # confusion matrix
    cm_elasticnet = np.round(
        confusion_matrix(y_test, y_pred, sample_weight=test_sample_weight) // 10 ** 6, 4
    )
    # heatmap
    fig, ax = plt.subplots(figsize=(6, 4))
    sns.heatmap(cm_elasticnet, annot=True, fmt="0.3", ax=ax, cmap="Blues")
    ax.set_xlabel("Predicted label")
    ax.set_xlabel("Predicted label")
    ax.xaxis.set_ticklabels(["under 50k", "over 50k"])
    ax.yaxis.set_ticklabels(["under 50k", "over 50k"])
    ax.set_title(f"Confusion Matrix of the {title} model (Millions of Persons)")
    return fig
